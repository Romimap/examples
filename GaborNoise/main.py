from random import random
import cv2
import numpy
import math as m

#Gabor Filter
def gabor(i, x, y, K, a, w, F):
    x = i[0] - x
    y = i[1] - y
    return K * m.exp(-m.pi * m.pow(a, 2) * (m.pow(x, 2) + m.pow(y, 2))) * m.cos(2 * m.pi * F * (x * m.cos(w) + y * m.sin(w)))
    
#Euclidian Distance
def dist(i, x, y):
    return m.sqrt(m.pow(i[0] - x, 2) + m.pow(i[1] - y, 2))

#Image
width, height  = 200, 300
image = numpy.zeros((width,height,3), numpy.uint8)

#Gabor filter parameters
K = .5 #amplitude
a = 0.04 #range
wmin, wmax = 0, 2 * m.pi #orientation
Fmin, Fmax = .08, .08 #frequency

#Impulsions
impulsionsCount = 500
impulsions = []
for i in range(impulsionsCount):
    x, y, w, f = random() * width, random() * height, random() * (wmax - wmin) + wmin, random() * (Fmax - Fmin) + Fmin
    impulsions.append((x, y, w, f))

#Begin
minv = 1000
maxv = -999
for x in range(width):
    for y in range(height):
        value = 0
        for i in impulsions:
            if dist(i, x, y) < (1 / a):
                value += gabor(i, x, y, K, a, i[2], i[3])
        minv = min(value, minv)
        maxv = max(value, maxv)
        image[x, y] = min(255, max(0, value * 64 + 128)) #HACK: clamping the values occludes the fact that there are a lot of overflows, <value> should be in the range of [-1, 1] but for some reason, can go as far as [-3, 3].

print(f'[{minv}, {maxv}]')

cv2.imwrite("noise.png", image, [])

